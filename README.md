# graphite

A library for developing Graph Neural Networks (GNNs) based on atomic structures.


## Requirements

* Atomic Simulation Environment (`ase`)
    * For reading atomic structures and computing the neighbor list (used to generate graphs).
* PyTorch-Geometric (PyG)
    * For implementing GNN operations and models.
    * As the name suggests, PyG depends on PyTorch (1.8.1 or newer is preferred).

I suggest using conda to first install PyTorch 1.8.1+ and `ase`, then install PyG.


## Usage

The purpose of `graphite` is two-fold:
1. to process or analyze atomic structures (using `ase`) in the context of GNNs,
2. and to define (graph) neural network operations or graph data formats that have not been included in PyTorch and PyG.

Therefore, `graphite` is meant to be a general toolbox of GNN codes (e.g., helper functions and uncommon graph convolutions) for atomic structures. Production-stage codes for specific applications or demonstrations should be hosted on separate, ad-hoc repositories.

Some examples for how to use `graphite` are shown below. You may also find application examples in the `scripts` folder.


### Convert atomic structures to PyG's `Data` object using ASE's neighbor list algorithm

```python
import ase.io
from graphite import atoms2graph
from torch_geometric.data import Data

atoms = ase.io.read('path/to/atoms.file')
x_atm = ...  # Define the featurization for the atoms
edge_index, edge_dist = atoms2graph(atoms, cutoff=3.5)
edge_attr = ...  # Define the featurization for the edges
data = Data(
    x          = torch.tensor(x_atm,      dtype=torch.float),
    edge_index = torch.tensor(edge_index, dtype=torch.long),
    edge_attr  = torch.tensor(edge_attr,  dtype=torch.float),
)
```

### Line graph construction

```python
from graphite import line_graph_bnd_ang, line_graph_dih_ang

edge_index_G = ... # Define your original graph G in terms of `edge_index` (this format is used by PyG)

# Construct the corresponding line graph L for bond angles
edge_index_L_bnd_ang = line_graph_bnd_ang(edge_index_G)

# Construct the corresponding "line graph" L' (not technically a line graph) for dihedral angles
edge_index_L_dih_ang = line_graph_dih_ang(edge_index_G)
```
