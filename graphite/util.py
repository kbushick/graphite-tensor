import numpy as np


__all__ = ['np_scatter']


def np_scatter(src, index, func=None):
    src, index = np.array(src), np.array(index)
    if func:
        return (func(src[index == i]) for i in np.unique(index))
    else:
        return (src[index == i] for i in np.unique(index))
