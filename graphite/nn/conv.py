import torch
from torch.nn           import Linear, LayerNorm, SiLU, Sigmoid
from torch_geometric.nn import MessagePassing
from torch_scatter      import scatter


__all__ = ['EGConv', 'EGConv_v2']


class EGConv(MessagePassing):
    def __init__(self, node_dim, edge_dim, epsilon=1e-5):
        super(EGConv, self).__init__(aggr='add')
        self.W_src  = Linear(node_dim, node_dim)
        self.W_dst  = Linear(node_dim, node_dim)
        self.W_e    = Linear(node_dim*2 + edge_dim, edge_dim)
        self.act    = SiLU()
        self.sigma  = Sigmoid()
        self.norm_x = LayerNorm([node_dim])
        self.norm_e = LayerNorm([edge_dim])
        self.eps    = epsilon

        self.reset_parameters()

    def reset_parameters(self):
        torch.nn.init.xavier_uniform_(self.W_src.weight); self.W_src.bias.data.fill_(0)
        torch.nn.init.xavier_uniform_(self.W_dst.weight); self.W_dst.bias.data.fill_(0)
        torch.nn.init.xavier_uniform_(self.W_e.weight);   self.W_e.bias.data.fill_(0)

    def forward(self, x, edge_index, edge_attr):
        i, j = edge_index

        # Calculate gated edges
        sigma_e = self.sigma(edge_attr)
        e_sum   = scatter(src=sigma_e, index=i , dim=0)
        e_gated = sigma_e / (e_sum[i] + self.eps)

        # Update the nodes (this utilizes the gated edges)
        out = self.propagate(edge_index, x=x, e_gated=e_gated)
        out = self.W_src(x) + out
        out = x + self.act(self.norm_x(out))

        # Update the edges
        z = torch.cat([x[i], x[j], edge_attr], dim=-1)
        edge_attr = edge_attr + self.act(self.norm_e(self.W_e(z)))

        return out, edge_attr

    def message(self, x_j, e_gated):
        return e_gated * self.W_dst(x_j)


class EGConv_v2(MessagePassing):
    def __init__(self, node_dim, edge_dim, epsilon=1e-5):
        super(EGConv_v2, self).__init__(aggr='add')
        self.W_src  = Linear(node_dim, node_dim)
        self.W_dst  = Linear(node_dim, node_dim)
        self.W_A    = Linear(node_dim, edge_dim)
        self.W_B    = Linear(node_dim, edge_dim)
        self.W_C    = Linear(edge_dim, edge_dim)
        self.act    = SiLU()
        self.sigma  = Sigmoid()
        self.norm_x = LayerNorm([node_dim])
        self.norm_e = LayerNorm([edge_dim])
        self.eps    = epsilon

        self.reset_parameters()

    def reset_parameters(self):
        torch.nn.init.xavier_uniform_(self.W_src.weight); self.W_src.bias.data.fill_(0)
        torch.nn.init.xavier_uniform_(self.W_dst.weight); self.W_dst.bias.data.fill_(0)
        torch.nn.init.xavier_uniform_(self.W_A.weight);   self.W_A.bias.data.fill_(0)
        torch.nn.init.xavier_uniform_(self.W_B.weight);   self.W_B.bias.data.fill_(0)
        torch.nn.init.xavier_uniform_(self.W_C.weight);   self.W_C.bias.data.fill_(0)

    def forward(self, x, edge_index, edge_attr):
        i, j = edge_index

        # Calculate gated edges
        sigma_e = self.sigma(edge_attr)
        e_sum   = scatter(src=sigma_e, index=i , dim=0)
        e_gated = sigma_e / (e_sum[i] + self.eps)

        # Update the nodes (this utilizes the gated edges)
        out = self.propagate(edge_index, x=x, e_gated=e_gated)
        out = self.W_src(x) + out
        out = x + self.act(self.norm_x(out))

        # Update the edges
        edge_attr = edge_attr + self.act(self.norm_e(self.W_A(x[i]) \
                                                   + self.W_B(x[j]) \
                                                   + self.W_C(edge_attr)))

        return out, edge_attr

    def message(self, x_j, e_gated):
        return e_gated * self.W_dst(x_j)
