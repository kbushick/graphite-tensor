import torch
from torch.nn      import Embedding, Linear, ModuleList, SiLU, Sequential
from torch_scatter import scatter
from ..basis       import RadialBesselFunc
from ..conv        import EGConv


class EGCNN(torch.nn.Module):
    def __init__(self, dim=100, num_interactions=6, num_species=3, cutoff=6.0):
        super(EGCNN, self).__init__()

        self.dim              = dim
        self.num_interactions = num_interactions
        self.cutoff           = cutoff

        self.embedding        = Embedding(num_species, dim)
        self.expand_bnd_dist  = RadialBesselFunc(dim, cutoff)

        self.atm_bnd_interactions = ModuleList()
        for _ in range(num_interactions):
            self.atm_bnd_interactions.append(EGConv(dim, dim))

        self.head = Sequential(
            Linear(dim, dim), SiLU(),
        )

        self.out = Sequential(
            Linear(dim, 3),
        )
        
        self.reset_parameters()
        
    def reset_parameters(self):
        self.embedding.reset_parameters()
        for interaction in self.atm_bnd_interactions:
            interaction.reset_parameters()
        
    def forward(self, data):
        edge_index = data.edge_index_G
        h_atm      = self.embedding(data.x_atm)
        h_bnd      = self.expand_bnd_dist(data.x_bnd)

        for i in range(self.num_interactions):
            h_atm, h_bnd = self.atm_bnd_interactions[i](h_atm, edge_index, h_bnd)

        h = scatter(h_atm, data.x_atm_batch, dim=0, reduce='add')
        h = self.head(h)
        return self.out(h), h_atm
    
    def __repr__(self):
        return (f'{self.__class__.__name__}('
                f'dim={self.dim}, '
                f'num_interactions={self.num_interactions}, '
                f'cutoff={self.cutoff})')
