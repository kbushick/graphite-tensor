import torch
from math import pi


__all__ = ['GaussianSmearing', 'RadialBesselFunc']


class GaussianSmearing(torch.nn.Module):
    def __init__(self, start=0, stop=15, num_gaussians=150, gamma=1):
        super(GaussianSmearing, self).__init__()
        mu = torch.linspace(start, stop, num_gaussians)
        self.gamma = gamma
        self.register_buffer('mu', mu)
        
    def forward(self, dist):
        dist_offset = dist.view(-1, 1) - self.mu.view(1, -1)
        return torch.exp(-self.gamma * torch.pow(dist_offset, 2))


class RadialBesselFunc(torch.nn.Module):
    def __init__(self, num_radial=16, cutoff=3.5):
        super(RadialBesselFunc, self).__init__()
        n = torch.arange(1, num_radial+1)
        c = torch.tensor(cutoff)
        self.register_buffer('n', n)
        self.register_buffer('c', c)

    def forward(self, d):
        return torch.sqrt(2/self.c) * torch.sin(self.n*pi*d / self.c) / d
