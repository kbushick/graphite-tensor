import numpy as np
import itertools
from functools import partial
from ase.neighborlist import neighbor_list
from .util import np_scatter


__all__ = [
    'atoms2graph',
    'line_graph_bnd_ang',
    'line_graph_dih_ang',
    'line_graph_dih_ang_v2',
    'get_bnd_angs',
    'get_dih_angs',
]


def atoms2graph(atoms, cutoff):
    i, j, d, = neighbor_list('ijd', atoms, cutoff)
    edge_index = np.stack((i, j))
    edge_dist  = d.reshape(-1, 1)
    # edge_dir   = D / np.linalg.norm(D, axis=1).reshape(-1, 1)
    return edge_index, edge_dist


permute_2 = partial(itertools.permutations, r=2)
def line_graph_bnd_ang(edge_index_G):
    src_G, dst_G = edge_index_G
    edge_index_L = [
        (u, v)
        for edge_pairs in np_scatter(np.arange(len(dst_G)), dst_G, permute_2)
        for u, v in edge_pairs
    ]
    return np.array(edge_index_L).T


def line_graph_dih_ang(edge_index_G):
    src_G, dst_G = edge_index_G
    edge_index_L = [
        (u, v)
        for u, (i, j) in enumerate(edge_index_G.T)
        for two_hop_dst in dst_G[(src_G == j) & (dst_G != i)]
        for v in ((dst_G == two_hop_dst) & (src_G != j)).nonzero()[0]
    ]
    return np.array(edge_index_L).T


def line_graph_dih_ang_v2(edge_index_G):
    src, dst = edge_index_G
    edge_index_L = [
        (u, v)
        for i, j in edge_index_G.T
        for u in np.flatnonzero((dst == i) & (src != j))
        for v in np.flatnonzero((dst == j) & (src != i))
    ]
    return np.array(edge_index_L).T


def get_bnd_angs(atoms, edge_index_G, edge_index_L_bnd_ang, cos_ang=False):
    indices  = edge_index_G.T[edge_index_L_bnd_ang.T].reshape(-1, 4)
    bnd_angs = atoms.get_angles(indices[:, [0, 1, 2]]).reshape(-1, 1)
    if cos_ang:
        return np.cos(np.radians(bnd_angs))
    else:
        return bnd_angs


def get_dih_angs(atoms, edge_index_G, edge_index_L_dih_ang, cos_ang=False):
    indices  = edge_index_G.T[edge_index_L_dih_ang.T].reshape(-1, 4)
    dih_angs = atoms.get_dihedrals(indices[:, [0, 1, 3, 2]]).reshape(-1, 1)
    if cos_ang:
        return np.cos(np.radians(dih_angs))
    else:
        return dih_angs
